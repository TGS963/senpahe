import sys
import download
import stream

print("senpahe")
print("Opened in Developer mode!")
print("Version: Dev Channel\n")

try:
    if len(sys.argv) <= 1:
        stream.main()
    elif sys.argv[1] == '-s':
        if len(sys.argv) <= 2:
            print("Please enter the name to be searched!!")
        else:
            tmp = ''
            for i in sys.argv[2:]:
                tmp = tmp + i + ' '
            if tmp[-1] == ' ':
                tmp = tmp[:-1]
            #print(tmp)
            download.main(1, tmp)
    elif sys.argv[1] == '-d':
        if len(sys.argv) <= 2:
            print("Please enter the link!!")
        else:
            download.main(2, sys.argv[2])
except KeyboardInterrupt:
    print("\nInterrupted by user.")
    sys.exit(0)
else:
    sys.exit(0)

